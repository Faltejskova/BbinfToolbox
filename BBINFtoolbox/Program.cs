﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace BBINFtoolbox
{
    class ScoringTab
    {
        private Dictionary<char, int> aaToIndex;

        private Dictionary<string, int> tab;
        public ScoringTab(Dictionary<string, int> tab, Dictionary<char, int> aaToIndex)
        {
            this.aaToIndex = aaToIndex;
            this.tab = tab;
        }

        public static ScoringTab LoadBlosum62(string path) //loads from file, file is provided.
        {
            var lines = File.ReadLines(path);
            var aminoacids = lines.First().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            var aatoindex = new Dictionary<char, int>();
            for (int k = 0; k < aminoacids.Length; k++)
            {
                aatoindex.Add(aminoacids[k][0], k);
            }

            var tab = new Dictionary<string, int>();
            int i = 1;
            foreach (var line in lines.Skip(1))
            {
                string[] splits = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                for (int j = 1; j <= i; j++)
                {
                    string code = $"{aminoacids[j - 1]}{splits[0]}";
                    tab.Add(code, int.Parse(splits[j]));
                }
                i++;
            }
            return new ScoringTab(tab, aatoindex);
        }
        
        public int Cost(char a, char b)
        {
            if (aaToIndex[a] > aaToIndex[b])
            {
                //prohodit
                char help = a;
                a = b;
                b = help;
            }
            string code = $"{a}{b}";
            return tab[code];
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            switch (args[0])
            {
                case "-1":
                    int molPosition = int.Parse(args[2]);
                    int subsqStart = int.Parse(args[3]);
                    int subsqLenght = int.Parse(args[4]);
                    loadAndExamineFasta(args[1], molPosition, subsqStart, subsqLenght);
                    break;
                case "-2":
                    if (args.Length == 2)
                    {
                        List<FastaModel> fastas = FastaParser.Parse(args[1]);
                        hammingDist(fastas[0], fastas[1]);
                    }
                    else
                    {
                        if (args.Length == 3)
                        {
                            FastaModel fasta1 = FastaParser.Parse(args[1])[0];
                            FastaModel fasta2 = FastaParser.Parse(args[2])[0];
                            hammingDist(fasta1, fasta2);
                        }
                        else
                            throw new ArgumentException();
                    }     
                    break;
                case "-3":
                    if (args.Length == 2)
                    {
                        List<FastaModel> fastas = FastaParser.Parse(args[1]);
                        editDstAlignment(fastas[0].Sequence, fastas[1].Sequence);
                    }
                    else
                    {
                        if (args.Length == 3)
                        {
                            FastaModel fasta1 = FastaParser.Parse(args[1])[0];
                            FastaModel fasta2 = FastaParser.Parse(args[2])[0];
                            editDstAlignment(fasta1.Sequence, fasta2.Sequence);
                        }
                        else
                            throw new ArgumentException();
                    }
                    break;
                case "-4":
                    loadAndExaminePDB(args[1]);
                    break;
                case "-5":
                    int? column = null;
                    if (args.Length == 4) column = int.Parse(args[3]);
                    loadAndExamineMSA(args[1], args[2], column);
                    break;
                case "-6":                   
                    int n = int.Parse(args[3]);
                    jsdForColumn(args[1], args[2], n);
                    break;
                case "-7":
                    int model = int.Parse(args[4]);
                    seqAndStruct(args[1], args[2], args[3], model);
                    break;

            }
            Console.WriteLine("Press enter to exit...");
            Console.ReadLine();
        }

        private static void seqAndStruct(string msaFile, string pdbFile, string posID, int pdbModel)
        {
            PdbFileRepresentation pdb = PDB.LoadFromFile(pdbFile);
            Conservation conserv = new Conservation(msaFile);
            List<Tuple<int, double>> result;
            int pos;
            if (int.TryParse(posID, out pos))
                result = conserv.GetConservationOfActiveSites(pdb, pos, pdbModel);
            else
                result = conserv.GetConservationOfActiveSites(pdb, posID, pdbModel);
            var msa = MultipleSeqAlignment.Load(msaFile);
            foreach (var item in result)
            {
                Console.Write($"position {item.Item1} - conservation {item.Item2} at column ");
                foreach (var stf in msa.GetColumn(item.Item1)) Console.Write(stf);
                Console.WriteLine();
            }
        }

        private static void jsdForColumn(string msaFromFile, string posID, int n)
        {
            var msa = MultipleSeqAlignment.Load(msaFromFile);
            var cons = new Conservation(msa);
            int pos;
            List<double> conservations;
            string sq;
            if (int.TryParse(posID, out pos))
            {
                conservations = cons.GetJSDConservationOfSeq(pos);
                sq = msa.GetSequence(pos);
            }
            else
            {
                conservations = cons.GetJSDConservationOfSeq(posID);
                sq = msa.GetSequence(posID);
            }
            int j = 0;
            for (int i = 0; i < conservations.Count; i++)
            {
                while (sq[j] == '-' || sq[j] == 'X')
                {
                    char tlap = sq[j];
                    j++;
                }
                
                Console.WriteLine($"{sq[j]} {conservations[i]}");
                j++;
            }
            Console.WriteLine();
            List<Tuple<int, double>> tops = cons.GetTopConservedPositions(n);
            Console.WriteLine($"Top {n} conserved positions:");
            foreach (var item in tops)
            {
                Console.WriteLine($"{item.Item1} - conservation {item.Item2}");
            }
        }

        private static void loadAndExaminePDB(string pdbFile)
        {
            PdbFileRepresentation pdb = PDB.LoadFromFile(pdbFile);
            Console.WriteLine("Overall information");
            Console.WriteLine($"number of models: {pdb.ModelsCount}");
            Console.WriteLine($"number of chains: {pdb.ChainCount}"); //todo
            Console.WriteLine($"number of residues: {pdb.ResidueCount}");
            Console.WriteLine($"number of heteroatoms: {pdb.Heteroatoms.Count}");
            Console.WriteLine($"number of atoms (except heteroatoms): {pdb.AtomCount}"); //todo

            Console.WriteLine("Model number? Insert and press enter.");
            int modelNum = int.Parse(Console.ReadLine());
            PdbModel model = pdb.GetModel(modelNum);
            Console.WriteLine($"Max width of the model: {model.GetMaxWidth()}");

            Console.WriteLine("Chain, residue number, atom number? Write all, divide with spaces.");
            string[] splits = Console.ReadLine().Split(' ');
            PdbChain chain = model.GetChain(splits[0]);
            Console.WriteLine($"chain {splits[0]} has {chain.Residues.Count} residues.");
            int resnum;
            PdbResidue res;
            if (int.TryParse(splits[1], out resnum))
                res = chain.GetResidue(resnum);
            else
                res = chain.GetResidue(splits[1]);
            Console.WriteLine($"residue {splits[1]} is {res.Name}, on {res.NumberInSequence} in sequence of model");
            PdbAtom atom = res.GetAtom(int.Parse(splits[2]));
            Console.WriteLine($"Atom is on coordinates {atom.Coordinates[0]} {atom.Coordinates[1]} {atom.Coordinates[2]}, type {atom.Type}, name {atom.Name}");
            Console.WriteLine($"        temperature factor {atom.TemperatureFactor}, occupancy {atom.Occupancy}, alternate location {atom.AlternateLoc}, charge {atom.Charge}");
            Console.WriteLine();

            Console.WriteLine("To get atoms and residues in radius, write heteroatom number and radius divided by spaces and press enter.");
            splits = Console.ReadLine().Split(' ');
            int hetAtNum = int.Parse(splits[0]);
            double radius = double.Parse(splits[1]);
            PdbAtom hetAt = pdb.GetHeteroatom(hetAtNum);
            var atomsInRad = model.GetAtomsInRadius(hetAt, radius);
            var resiInRad = model.GetResiduesInRadius(hetAt, radius);
            Console.WriteLine();
            Console.WriteLine("Atoms in radius:");
            foreach (var at in atomsInRad)
            {
                Console.WriteLine($"{at.Number} {at.Name} {at.Coordinates[0]} {at.Coordinates[1]} {at.Coordinates[2]}");
            }
            Console.WriteLine();
            Console.WriteLine("Residues in radius:");
            foreach (var resi in resiInRad)
            {
                Console.WriteLine($"{resi.Name} {resi.Number}");
            }
        }

        private static void loadAndExamineMSA(string msaFile, string posID, int? column)
        {
            MultipleSeqAlignment msa = MultipleSeqAlignment.Load(msaFile);
            int sqCount = msa.GetSequenceCount();
            for (int i = 0; i < sqCount; i++)
            {
                Console.WriteLine($"Sequence {i}: {msa.GetSequence(i)}");
            }
            int pos;
            string selected;
            if (int.TryParse(posID, out pos))
            {
                selected = msa.GetSequence(pos);
            }
            else
                selected = msa.GetSequence(posID);
            Console.WriteLine();
            Console.WriteLine("Sequence on selected position: ");
            Console.WriteLine(selected);
            if (column == null) column = 0;
            Console.WriteLine($"Column {column}:");
            Console.WriteLine(msa.GetColumn((int)column));

            ScoringTab blosum62 = ScoringTab.LoadBlosum62("blosum62.txt");

            Console.WriteLine($"Column {column} sum of pairs: {msa.GetSumOfPairs((int)column, (a, b) => blosum62.Cost(a, b)) }"); //todo scoring table
            Console.WriteLine($"Total sum of pairs: {msa.GetSumOfPairsTotal((a, b) => blosum62.Cost(a, b))}");
        }

        private static void editDstAlignment(string fasta1, string fasta2)
        {
            var editAlign = new SeqAlignment(fasta1, fasta2);
            Console.WriteLine($"Edit distance: {editAlign.EditDst()}");
            var alignments = editAlign.GetAllOptimalAlignments();
            Console.WriteLine("Alignments:");
            int i = 0;
            foreach (SeqAlignmentRepresentation align in alignments)
            {
                Console.WriteLine($"Alignemnt {i}:");
                Console.WriteLine(align.Seq1Aligned);
                Console.WriteLine(align.Seq2Aligned);
                Console.WriteLine();
                i++;
            }
        }

        private static void hammingDist(FastaModel fasta1, FastaModel fasta2)
        {
            int ret = Distance.HammingDst(fasta1, fasta2);
            Console.WriteLine($"Hamming distance: {ret}");
        }

        private static void loadAndExamineFasta(string fastaFile, int position, int subsqStart, int subsqLenght)
        {
            var fastas = FastaParser.Parse(fastaFile);
            foreach (var fasta in fastas)
            {
                
                Console.WriteLine($"description: {fasta.Description}");
                Console.WriteLine($"sequence: {fasta.Sequence}");
                Console.WriteLine($"lenght {fasta.Lenght}");
            }
            Console.WriteLine("Subsequence:");
            Console.WriteLine($"{fastas[position].GetSubsequence(subsqStart, subsqLenght)}");
        }
    }
}

