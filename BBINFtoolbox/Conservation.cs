﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBINFtoolbox
{
    public class Conservation
    {
        private static Dictionary<char, int> aaToIndex = new Dictionary<char, int>()
        {
            { 'A', 0},
            { 'R', 1},
            { 'N', 2},
            { 'D', 3},
            { 'C', 4},
            { 'Q', 5},
            { 'E', 6},
            { 'G', 7},
            { 'H', 8},
            { 'I', 9},
            { 'L', 10},
            { 'K', 11},
            { 'M', 12},
            { 'F', 13},
            { 'P', 14},
            { 'S', 15},
            { 'T', 16},
            { 'W', 17},
            { 'Y', 18},
            { 'V', 19},
            { 'B', 20},
            { 'Z', 21},
            { 'X', 22}
        };


        private static double[] backgroundDistributions = new double[]
        {
            0.078,
            0.051,
            0.041,
            0.052,
            0.024,
            0.034,
            0.059,
            0.083,
            0.025,
            0.062,
            0.092,
            0.056,
            0.024,
            0.044,
            0.043,
            0.059,
            0.055,
            0.014,
            0.034,
            0.072
        };

        double[] sequenceWeights = null;
        MultipleSeqAlignment msa;

        public Conservation(string msaFromFile)
        {
            msa = MultipleSeqAlignment.Load(msaFromFile);
        }

        public Conservation(MultipleSeqAlignment msa)
        {
            this.msa = msa;
        }

        private double[] calculateSequenceWeights()
        {
            var seqWeights = new double[msa.GetSequenceCount()];
            
            int msaLenght = msa.GetSequence(0).Length;

            for (int i = 0; i < msaLenght; i++)
            {
                var freqCounts = new double[20];
                char[] column = msa.GetColumn(i);
                for (int j = 0; j < msa.GetSequenceCount(); j++)
                {
                    if (column[j] != '-' && column[j] != 'X')
                        freqCounts[aaToIndex[column[j]]]++;
                }
                int observedCount = freqCounts.Where(t => t > 0).Count();
                for (int j = 0; j < msa.GetSequenceCount(); j++)
                {
                    if (column[j] != '-' && column[j] != 'X') //X - undefined
                    {
                        double d = freqCounts[aaToIndex[column[j]]] * observedCount;
                        seqWeights[j] += 1 / d;
                    }
                }
            }

            for (int i = 0; i < seqWeights.Length; i++)
            {
                seqWeights[i] /= msaLenght;
            }
            return seqWeights;    
        }

        private double relativeEntropy(double[] first, double[] second)
        {
            double result = 0;
            if (first.Length != second.Length) throw new ArgumentException();
            for (int i = 0; i < first.Length; i++)
            {
                if (second[i] != 0)
                {
                    if (first[i] != 0)
                        result += first[i] * Math.Log((first[i] / second[i]), 2);
                }
            }
            return result;
        }

        private double[] probabilitiesInColumn(int columnNum)
        {          
            var result = new double[20];
            var column = msa.GetColumn(columnNum);
            for (int i = 0; i < column.Length; i++)
            {
                if (column[i] != '-' && column[i] != 'X') result[aaToIndex[column[i]]] += sequenceWeights[i];
            }
            double sum = sequenceWeights.Sum();
            for (int i = 0; i < result.Length; i++)
            {
                result[i] /= (sum + 20 * 0.000001);
            }
            return result;
        }

        private double weightedGapCount(int columnNum)
        {
            char[] column = msa.GetColumn(columnNum);
            double result = 0;
            for (int i = 0; i < msa.GetSequenceCount(); i++)
            {
                if (column[i] == '-')
                    result += sequenceWeights[i];
            }
            result /= sequenceWeights.Sum();
            return 1 - result;
        }

        private double getSqConservation(int columnPos)//jsd - source : https://academic.oup.com/bioinformatics/article/23/15/1875/203579
        {
            if (sequenceWeights == null) sequenceWeights = calculateSequenceWeights();

            double[] pc = probabilitiesInColumn(columnPos);
            double[] r = new double[20];

            for (int i = 0; i < pc.Length; i++)
            {
                r[i] = 0.5 * (pc[i] + backgroundDistributions[i]);
            }

            double jsd = 0.5 * (relativeEntropy(pc, r) + relativeEntropy(backgroundDistributions, r));

            return jsd * weightedGapCount(columnPos);
        }

        private List<double> conservationToSq(string sq)
        {
            var result = new List<double>();
            for (int i = 0; i < sq.Length; i++)
            {
                if (sq[i] != '-')
                {
                    result.Add(getSqConservation(i));
                }
            }
            return result;
        }

        /// <summary>
        /// Returns Jensen-Shannon divergence of position of user-defined sequence (without considering gaps in this sequence given by source MSA).
        /// </summary>
        /// <param name="seqPosition">Zero-based position of sequence of interest in MSA</param>
        /// <returns></returns>
        public List<double> GetJSDConservationOfSeq(int seqPosition)
        {
            var sequence = msa.GetSequence(seqPosition);
            return conservationToSq(sequence); //without gaps
        }
        /// <summary>
        /// Returns Jensen-Shannon divergence of position of user-defined sequence (without considering gaps in this sequence given by source MSA).
        /// </summary>
        /// <param name="seqName">Name of sequence of interest used in MSA.</param>
        /// <returns></returns>
        public List<double> GetJSDConservationOfSeq(string seqName)
        {
            var sequence = msa.GetSequence(seqName);
            return conservationToSq(sequence);
        }
        /// <summary>
        /// Counts conservation scores for all positions in MSA and returns the specified ammount of top scoring ones.
        /// </summary>
        /// <param name="howMany">Ammount of top-scoring positions to return.</param>
        /// <returns>Ordered list of most scoring positions.</returns>
        public List<Tuple<int, double>> GetTopConservedPositions(int howMany)
        {
            int msalenght = msa.GetSequence(0).Length;
            var tops = new List<Tuple<int, double>>();

            for (int i = 0; i < msalenght; i++)
            { 
                double cons = getSqConservation(i);
                tops.Add(new Tuple<int, double>(i, cons));
            }
            return tops.OrderByDescending(a => a.Item2).Take(howMany).ToList();
        }
        /// <summary>
        /// Gets residues corresponding to active sites (residues in 2.5 A from ligands specified in PDB file) and counts conservation of these residues.
        /// </summary>
        /// <param name="pdb">Loaded PDB file.</param>
        /// <param name="structName">Name of sequence of interest used in MSA.</param>
        /// <param name="model">zero-based identifier of model user wishes to use</param>
        /// <returns>List of position - conservation tuples for all heteroatoms.</returns>
        public List<Tuple<int, double>> GetConservationOfActiveSites(PdbFileRepresentation pdb, string structName, int model)
        {
            return getConservationActiveSites(pdb, msa.GetSequence(structName), model);
        }
        /// <summary>
        /// Gets residues corresponding to active sites (residues in 2.5 A from ligands specified in PDB file) and counts conservation of these residues.
        /// </summary>
        /// <param name="pdb">Loaded PDB file.</param>
        /// <param name="structNum">Zero-based position of sequence of interest in MSA</param>
        /// <param name="model">zero-based identifier of model user wishes to use</param>
        /// <returns>List of position - conservation tuples for all heteroatoms.</returns>
        public List<Tuple<int, double>> GetConservationOfActiveSites(PdbFileRepresentation pdb, int structNum, int model)
        {
            return getConservationActiveSites(pdb, msa.GetSequence(structNum), model);
        }

        private List<Tuple<int, double>> getConservationActiveSites(PdbFileRepresentation pdb, string sequence, int modelnum)
        {
            var heteroAtoms = pdb.Heteroatoms;
            var model = pdb.Models[modelnum];
            var result = new List<Tuple<int, double>>();
            foreach (var hetat in heteroAtoms)
            {
                var residues = model.GetResiduesInRadius(hetat, 2.5d);
                foreach (var resi in residues)
                {
                    int msaIndex = indexInMSA(resi.NumberInSequence, sequence);
                    char tl = sequence[msaIndex];
                    double cons = getSqConservation(msaIndex);
                    result.Add(new Tuple<int, double>(msaIndex, cons));
                }
            }
            return result;
        }

        private int indexInMSA(int residueNmb, string sq)
        {
            int notGapCount = 0; //how many not-gaps are passed
            for (int i = 0; i < sq.Length; i++)
            {
                if (sq[i] == '-') continue;
                if (notGapCount == residueNmb)
                {
                    return i;
                }

                notGapCount++;
            }
            return -1;
        }
           
    }
}
