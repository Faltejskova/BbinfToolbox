﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Runtime.CompilerServices;
[assembly: InternalsVisibleTo("BBinfToolboxTestPr")]

namespace BBINFtoolbox
{
    public class PdbFileRepresentation //todo lepsi jmeno
    {
        public PdbModel GetModel(int number)
        {
            return Models[number];
        }

        internal PdbFileRepresentation(List<PdbModel> models, List<PdbAtom> hetAtm)
        {
            Models = models;
            Heteroatoms = hetAtm;
            var chains = models.Select(model => model.ChainIdents);
            ChainCount = chains.Count();
            int resCount = 0;
            int atomsCount = 0;
            foreach (var model in Models)
            {
                foreach (var chainID in model.ChainIdents)
                {
                    resCount += model.GetChain(chainID).Residues.Count;
                    atomsCount += model.GetChain(chainID).Residues.Sum(res => res.Atoms.Count);
                }
            }
            ResidueCount = resCount;
            AtomCount = atomsCount;
        }

        public PdbAtom GetHeteroatom(int number)
        {
            return Heteroatoms.Find(at => at.Number == number);
        }

        public int AtomCount
        {
            get; private set;
        }

        public int ResidueCount //over all chains and models
        {
            get; private set;
        }

        public int ChainCount
        {
            get; private set;
        }

        public int ModelsCount
        {
            get { return Models.Count; }
        }

        public List<PdbModel> Models { get; private set; }
        public List<PdbAtom> Heteroatoms { get; private set; }
    }

    public class PDB
    {
        private static readonly Dictionary<string  , char> aminoAcidsShortcuts = new Dictionary<string, char>
        {
            { "ALA", 'A' },
            { "ARG", 'R' },
            { "ASN", 'N'  },
            { "ASP", 'D'  },
            { "CYS", 'C'  },
            { "GLU", 'E'  },
            { "GLN", 'Q' },
            { "GLY", 'G'  },
            { "HIS", 'H'  },
            { "ILE", 'I'  },
            { "LEU", 'L'  },
            { "LYS", 'K'  },
            { "MET", 'M' },
            { "PHE", 'F'  },
            { "PRO", 'P'  },
            { "SER", 'S' },
            { "THR", 'T'  },
            { "TRP",'W'   },
            { "TYR", 'Y'  },
            { "VAL", 'V'  }
        };
        /// <summary>
        /// Loader class.
        /// </summary>
        /// <param name="fromFile">Name of the source file.</param>
        /// <returns></returns>
        public static PdbFileRepresentation LoadFromFile(string fromFile)
        {
            var lines = File.ReadLines(fromFile).SkipWhile(
                t => !(Regex.IsMatch(t, "^ATOM.*")) && !(Regex.IsMatch(t, "^HETATM.*")) && !(Regex.IsMatch(t, "^MODEL.*")));
            List<PdbModel> models = new List<PdbModel>();
            List<PdbAtom> hetAtms = new List<PdbAtom>();
            // var atom = new PdbAtom(line.Replace('.', ',')); //cesky standart, pak blbne parsovani
            //18-20        Residue name  resName Residue name.
            //22           Character     chainID Chain identifier.
            //23-26        Integer resSeq       Residue sequence number.
            PdbResidue res = new PdbResidue();
            PdbChain chain = new PdbChain();
            PdbModel model = new PdbModel();
            StringBuilder builder = new StringBuilder();
            int numInsq = 0;
            foreach (var line in lines)
            {
                if (Regex.IsMatch(line, "^ATOM.*"))
                {
                    string textNmb = line.Substring(22, 4).Trim();
                    //read residue nmb - new?
                    var atom = new PdbAtom(line);
                    string newChain = line.Substring(21, 1);
                    if (res.NumberText != null && res.NumberText != textNmb)
                    {
                        //change - add residue to chain, 
                        chain.Residues.Add(res); //k byvalemu chainu bude napojeno byvaly residue, kdyz bude zaroven novy chain, nove residuum pujde k nemu
                        res = new PdbResidue();
                        res.Name = line.Substring(17, 3).Trim();
                        res.NumberText = textNmb;
                        res.NumberInSequence = numInsq;
                        numInsq++;
                        int numb;
                        if (int.TryParse(textNmb, out numb))
                        {
                            res.Number = numb;
                        }                                          
                    }
                    if (res.NumberText == null)
                    {
                        int numb;
                        res.NumberText = textNmb;
                        if (int.TryParse(textNmb, out numb))
                        {
                            res.Number = numb;
                        }
                        res.NumberInSequence = numInsq;
                        numInsq++;
                        res.Name = line.Substring(17, 3).Trim();
                    }

                    if (chain.Ident != "" && chain.Ident != newChain)
                    {
                        //change
                        model.AddChain(chain);
                        chain = new PdbChain();
                        chain.Ident = line[21].ToString();
                    }
                    if (chain.Ident == "")
                        chain.Ident = newChain;
                   
                    res.Atoms.Add(atom);
                    //read chain ident - new?
                    continue;
                }
                if (Regex.IsMatch(line, "^TER.*") || Regex.IsMatch(line, "^ENDMDL.*"))
                {
                    numInsq = 0;
                    chain.Residues.Add(res);
                    model.AddChain(chain);
                    models.Add(model);
                    model = new PdbModel();
                    chain = new PdbChain();
                    res = new PdbResidue();
                    continue;
                }
                if (Regex.IsMatch(line, "^HETATM.*"))
                {
                    var het = new PdbAtom(line);
                    hetAtms.Add(het);
                    continue;
                }
            }
            if (res.Number != -1)
            {
                chain.Residues.Add(res);
            } //add residue
            if (chain.Ident != "")
            {
                model.AddChain(chain);
            }
            if (model.ChainIdents.Count != 0) 
                models.Add(model);    
            var structure = new PdbFileRepresentation(models, hetAtms);
            return structure;
        }
    }

    public class PdbChain
    {
        internal PdbChain() { }
        public List<PdbResidue> Residues { get; internal set; } = new List<PdbResidue>();
        public string Ident { get; internal set; } = "";
        public PdbResidue GetResidue(int residueNmb)
        {
            return Residues.Find(r => r.Number == residueNmb);
        }
        public PdbResidue GetResidue(string residueTextNmb)
        {
            return Residues.Find(r => r.NumberText == residueTextNmb);
        }
    }

    public class PdbModel
    {
        internal void AddChain(PdbChain chain)
        {
            chains.Add(chain);
            AtomsCount += chain.Residues.SelectMany(a => a.Atoms).Count();
        }

        internal PdbModel() {}

        public int AtomsCount { get; private set; }    

        List<PdbChain> chains = new List<PdbChain>();

        double width = -1f;

        public List<string> ChainIdents
        {
            get
            {
                return chains.Select(a => a.Ident).ToList();
            }
        }

        public PdbChain GetChain(string chainIdentifier)
        {            
            return chains.Find(a => a.Ident == chainIdentifier);
        }

        public double GetMaxWidth()
        {
            if (width != -1d) return width; //delsi vypocet -> predpocet
            width = -1d;
            var atoms = chains.SelectMany(a => a.Residues.SelectMany(r => r.Atoms)).ToList();
            for (int i = 0; i < AtomsCount; i++)
            {
                for (int j = i + 1; j < AtomsCount; j++)
                {
                    var dist = pointDistanceKvadrate(atoms[i].Coordinates, atoms[j].Coordinates);
                    if (dist > width) width = dist;
                }
            }
            width = Math.Sqrt(width);
            return width;
        }

        private double pointDistanceKvadrate(double[] x, double[] y)
        {
            if (x.Length != y.Length) throw new ArgumentException("Cannot work with 2 vectors of nonequal dimension.");
            double distance = 0;
            for (int i = 0; i < x.Length; i++)
            {
                distance += (x[i] - y[i]) * (x[i] - y[i]);
            }
            //distance = Math.Sqrt(distance);
            //if (distance < 10) { }
            return distance;
        }

        public List<PdbAtom> GetAtomsInRadius(PdbAtom fromLigand, double distance)
        {
            double distanceKvadrate = distance * distance;
            var atoms = chains.SelectMany(a => a.Residues.SelectMany(r => r.Atoms));
            var result = new List<PdbAtom>();
            result = atoms.Where(atom => pointDistanceKvadrate(atom.Coordinates, fromLigand.Coordinates) <= distanceKvadrate).ToList();
            return result;
        }
        /// <summary>
        /// Get residues that have at least 1 atom in radius
        /// </summary>
        /// <param name="fromLigand"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        public List<PdbResidue> GetResiduesInRadius(PdbAtom fromLigand, double distance)
        {
            double distanceKvadrate = distance * distance;
            var result = new List<PdbResidue>();
            var residues = chains.SelectMany(a => a.Residues);
            foreach (var resi in residues)
            {
                foreach (var atom in resi.Atoms)
                {
                    var currentDist = pointDistanceKvadrate(fromLigand.Coordinates, atom.Coordinates);
                    if (currentDist <= distanceKvadrate)
                    {
                        result.Add(resi);
                        break;
                    }
                }
            }
            return result;
        }
    }

    public class PdbResidue
    {
        internal PdbResidue() { }
        public int NumberInSequence { get; internal set; }
        public string Name { get; internal set; }
        public int Number { get; internal set; } = -1;
        public string NumberText { get; internal set; } = null;
        public PdbAtom GetAtom(int number)
        {
            return Atoms.Find(a => a.Number == number);
        }
        public List<PdbAtom> Atoms { get; internal set; } = new List<PdbAtom>();
    }

    public class PdbAtom
    {
        internal PdbAtom(string line)
        {
            line = line.Replace('.', ',');
            //todo chybovy vstupy = stringy budou v pohode
            int nmb;
            if (int.TryParse(line.Substring(6, 5).Trim(), out nmb))
            {
                Number = nmb;
            }
            else
            {
                //todo exception here? what? decide!
            }

            Name = line.Substring(12, 4).Trim();
            AlternateLoc = line[16];

            //at to hazi vyjimky, kdyz to nema koordinaty...
            Coordinates = new double[]
                {
                    double.Parse(line.Substring(30, 8).Trim()),
                    double.Parse(line.Substring(38, 8).Trim()),
                    double.Parse(line.Substring(46, 8).Trim())
                };

            Occupancy = double.Parse(line.Substring(54, 6).Trim());

            TemperatureFactor = double.Parse(line.Substring(60, 6).Trim());

            Type = line.Substring(76, 2).Trim();
            if (line.Length < 78)
                Charge = "";
            else
                Charge = line.Substring(78, 2).Trim();
        }
//1BASED! 
        public int Number { get; private set; } //7 - 11
        public string Name { get; private set; } // 13 - 16
        public char AlternateLoc { get; private set; } //17
        public double[] Coordinates { get; private set; } //31 - 38, 39 - 46, 47 - 54        
        public double Occupancy { get; private set; } //55 - 60
        public double TemperatureFactor { get; private set; } //61 - 66
        public string Type { get; private set; } //77 - 78
        public string Charge { get; private set; } //79 - 80       
    }
}
