﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBINFtoolbox
{
    public class Storage<T>
    {
        private Dictionary<int, T> storage = new Dictionary<int, T>();
        int ident = 0;
        internal void Store(T model)
        {
            storage.Add(ident, model);
            ident++;
        }
        public T GetModel(int ident)
        {
            if (this.ident <= ident) throw new ArgumentOutOfRangeException();
            return storage[ident];
        }
    }
}
