﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BBINFtoolbox
{
    public class FastaParser
    {
        public static List<FastaModel> Parse(string fromFile)
        {
            var storage = new List<FastaModel>();
            parse(fromFile, storage);
            return storage;
        }

        public static List<FastaModel> Parse(string fromFile, List<FastaModel> fastaStorage)
        {
            parse(fromFile, fastaStorage);
            return fastaStorage;
        }

        private static void parse(string fromFile, List<FastaModel> storage)
        {
            //multifasta! - rozdelovac je >desc
            var lines = File.ReadLines(fromFile);
            FastaModel model = null;
            StringBuilder buff = new StringBuilder();
            foreach (var line in lines)
            {
                if (line.Length == 0) continue;
                if (line[0] == '>')
                {
                    if (model != null)
                    {
                        //model.Sequence = buffer.ToString();
                        //buffer.Clear();
                        model.Sequence = buff.ToString();
                        storage.Add(model);
                        buff.Clear();
                    }
                    model = new FastaModel();
                    model.Description = line.Substring(1);
                }
                else
                {
                   // Console.WriteLine(line.Length);
                    buff.Append(line);
                    //model.Sequence.Add(line);
                }
            }

            if (model != null)
                model.Sequence = buff.ToString();
                storage.Add(model);
            buff.Clear();
        }
    }

    public class FastaModel : ISequenceRepresentation
    {
        internal FastaModel() { }
        public int Lenght { get
            {
                // lenght = Sequence.Sum(t => t.Length);
                return Sequence.Length;
            }
        }

        public string Sequence
        {
            get
            {
                if (sequence == null) sequence = "";//new List<string>();
                return sequence;
            }
         internal set
            {
                sequence = value;
            }
        }
        string sequence;
        //lenght of genetic code could be problematic... 
        //not suitable for whole genom, but proteins should be problem free 
        //(c# can manage more than million chars in a string)

        public string Description { get; internal set; }

        public string GetSubsequence(int firstPosition, int subsqLenght)
        {
            if (firstPosition + subsqLenght - 1 > Lenght) throw new ArgumentOutOfRangeException();
            return Sequence.Substring(firstPosition, subsqLenght);          
        }
    }
}
