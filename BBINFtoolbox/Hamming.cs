﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBINFtoolbox
{
    public interface ISequenceRepresentation
    {
        string Sequence { get; }
        int Lenght { get; } //of the sequence
    }

    public class Distance
    {
        public static int HammingDst(ISequenceRepresentation sq1, ISequenceRepresentation sq2)
        {
            if (sq1.Lenght != sq2.Lenght) throw new ArgumentException();
            int result = 0;

            var sq1rep = sq1.Sequence;
            var sq2rep = sq2.Sequence;

            for (int i = 0; i < sq1.Lenght; i++)
            {
                if (sq1rep[i] != sq2rep[i])
                    result++;
            }

            return result;
        } 
    }

    public class SeqAlignmentRepresentation
    {
        internal SeqAlignmentRepresentation(string sq1, string sq2)
        {
            Seq1Aligned = sq1;
            Seq2Aligned = sq2;
        }
        public string Seq1Aligned { get; private set; }
        public string Seq2Aligned { get; private set; }
    }

    public class SeqAlignment
    {
        int gapPenalty = 1;
        string sq1;
        string sq2;
        public SeqAlignment(string sq1, string sq2)
        {
            this.sq1 = sq1;
            this.sq2 = sq2;
        }
        //sq1 X sq2
        int[,] tab = null;

        private int min(int a, int b, int c)
        {
            if (a < b && a < c) return a;
            if (b < c) return b;
            return c;
        }

        private int cost(char a, char b) //for edit distance, can be easily altered for specific 
        {
            if (a == b) return 0;
            return 1;
        }

        private void fillTab(int gapPenalty)
        {
            tab = new int[sq1.Length + 1, sq2.Length + 1];
            for (int i = 1; i <= sq1.Length; i++) tab[i, 0] = i * gapPenalty; //gap penalty
            for (int j = 1; j <= sq2.Length; j++) tab[0, j] = j * gapPenalty; //gap penalty

            for (int i = 1; i <= sq1.Length; i++)
                for (int j = 1; j <= sq2.Length; j++)
                {
                    tab[i, j] = min(gapPenalty + tab[i - 1, j],
                               gapPenalty + tab[i, j - 1],
                            cost(sq1[i - 1], sq2[j - 1]) + tab[i - 1, j - 1]);
                }
        } 
        /// <summary>
        /// Counts edit distance of two sequnces using dynamic programming.
        /// </summary>
        /// <returns>edit distance of sequences</returns>
        public int EditDst()
        {
            if (tab == null) fillTab(gapPenalty);
            return tab[sq1.Length, sq2.Length];
        }
        /// <summary>
        /// Gets all optimal alignment via backtracking in dynamic programming matrix.
        /// </summary>
        /// <returns>all alignments of two sequnces</returns>
        public List<SeqAlignmentRepresentation> GetAllOptimalAlignments()
        {
            if (tab == null) fillTab(gapPenalty);
            var sq1Buff = new StringBuilder();
            var sq2Buff = new StringBuilder();
            var result = getAlignments(sq1.Length, sq2.Length, true, sq1Buff, sq2Buff);
            return result;
        }

        /// <summary>
        /// Gets a single optimal alignment via backtracking in dynamic programming matrix.
        /// </summary>
        /// <returns>alignment of two sequences</returns>
        public SeqAlignmentRepresentation GetOptimalAlignment()
        {
            if (tab == null) fillTab(gapPenalty);
            //backtracking
            var sq1Buff = new StringBuilder();
            var sq2Buff = new StringBuilder();
            var result = getAlignments(sq1.Length, sq2.Length, false, sq1Buff, sq2Buff).Single();
            return result;
        }

        private List<SeqAlignmentRepresentation> getAlignments(int fromI, int fromj, bool wantAll, 
            StringBuilder sq1Buff, StringBuilder sq2Buff)
        {         
            var toReturn = new List<SeqAlignmentRepresentation>();

            int i = fromI;
            int j = fromj;

            int nextI = -1;
            int nextJ = -1;
            //Console.WriteLine("Commencing while...");
            while (i != 0 || j != 0)
            {
                bool set = false;
               // Console.WriteLine($" i = {i}, j = {j}");
                char toAdd1 = ' ';
                char toAdd2 = ' ';
                if ((i > 0 && j > 0) && tab[i - 1, j - 1] + cost(sq1[i -1], sq2[j-1]) == tab[i, j]) //alignable, default
                {
                    set = true;
                    nextI = i - 1;
                    nextJ = j - 1;
                    toAdd1 = sq1[i - 1];
                    toAdd2 = sq2[j - 1];
                }
                if (i > 0 && tab[i - 1, j] + gapPenalty == tab[i, j]) //sq2 gap
                {
                    if (set)
                    {
                        if (wantAll)
                        {
                            var newsq1Buff = new StringBuilder(sq1Buff.ToString() + sq1[i - 1]);
                            var newsq2Buff = new StringBuilder(sq2Buff.ToString() + '-');
                            var addition = getAlignments(i - 1, j, true, newsq1Buff, newsq2Buff);
                            toReturn.AddRange(addition);
                        }
                        else continue;
                    }
                    else
                    {
                        set = true;
                        nextI = i - 1;
                        nextJ = j;
                        toAdd1 = sq1[i - 1];
                        toAdd2 = '-';
                    }
                }
                if (j > 0 && tab[i, j - 1] + gapPenalty == tab[i, j]) //sq1 gap
                {
                    if (set)
                    {
                        var newsq1Buff = new StringBuilder(sq1Buff.ToString() + '-');
                        var newsq2Buff = new StringBuilder(sq2Buff.ToString() + sq2[j - 1]);
                        var addition = getAlignments(i, j - 1, true, newsq1Buff, newsq2Buff);
                        toReturn.AddRange(addition);
                    }
                    else
                    {
                        set = true;
                        nextI = i;
                        nextJ = j - 1;
                        toAdd1 = '-';
                        toAdd2 = sq2[j - 1];
                    }
                }
                sq1Buff.Append(toAdd1);
                sq2Buff.Append(toAdd2);
                i = nextI;
                j = nextJ;
                //Console.WriteLine($"adding {toAdd1} to sq1, {toAdd2} to sq2");
            }
            var str1 = sq1Buff.ToString();
            var str2 = sq2Buff.ToString();
            StringBuilder seq1 = new StringBuilder();
            StringBuilder seq2 = new StringBuilder();
            for (int c = str1.Length - 1; c >=0; c--)
            {
                seq1.Append(str1[c]);
            }
            for (int c = str2.Length - 1; c >= 0; c--)
            {
                seq2.Append(str2[c]);
            }
            toReturn.Add(
                new SeqAlignmentRepresentation(seq1.ToString(), seq2.ToString())
                );
            return toReturn;
        }
    }
}
