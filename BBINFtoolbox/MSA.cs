﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace BBINFtoolbox
{
    public class MultipleSeqAlignment
    {
        public static MultipleSeqAlignment Load(string fromFile)
        {
            var separ = new char[] { ' ' };
            var res = new MultipleSeqAlignment();
            //var tlap = new StreamReader(fromFile);
            var lines = File.ReadLines(fromFile).Skip(1).SkipWhile(t => t.Length == 0 || t[0] == ' ');
            List<string> names = new List<string>();
            List<StringBuilder> builders = new List<StringBuilder>();
            bool namesDictDone = false;
            int sqNum = 0;

            foreach (var line in lines)
            {
                if (line.Length == 0 || line[0] == ' ')
                {
                    namesDictDone = true;
                    sqNum = 0;
                    continue;
                }
                var items = line.Split(separ, StringSplitOptions.RemoveEmptyEntries);
                if (!namesDictDone)
                {
                    names.Add(items[0]);
                    builders.Add(new StringBuilder());
                }
                builders[sqNum].Append(items[1]);
                sqNum++;
            }

            for (int i = 0; i < names.Count; i++)
            {
                res.namesToPosition.Add(names[i], i);
                res.alignedSeq.Add(builders[i].ToString());
            }

            return res;
        }

        List<string> alignedSeq = new List<string>(); //poradi konstantni s poradim v souboru
        Dictionary<string, int> namesToPosition = new Dictionary<string, int>();

        public string GetSequence(int position) //zero-based
        {
            return alignedSeq[position];
        }

        public string GetSequence(string seqName)
        {
            int position = namesToPosition[seqName];
            return alignedSeq[position];
        }

        public char[] GetColumn(int position) //zero-based
        {
            if (position < 0 || position >= alignedSeq[0].Length) throw new ArgumentOutOfRangeException(); //vsechny jsou ted uz stejne dlouhy...
            var ret = new char[alignedSeq.Count];
            for (int i = 0; i < alignedSeq.Count; i++)
            {
                ret[i] = alignedSeq[i][position];
            }
            return ret;
        }

        public int GetSequenceCount()
        {
            return alignedSeq.Count;
        }

        private int sumOfPairs(int position, Func<char, char, int> costFunc)
        {
            int result = 0;
            for (int i = 0; i <= alignedSeq.Count; i++)
            {
                for (int j = i + 1; j < alignedSeq.Count; j++)
                {
                    result += costFunc(alignedSeq[i][position], alignedSeq[j][position]);
                }
            }
            return result;
        }

        public int GetSumOfPairs(int position, Func<char, char, int> costFunc)
        {
            if (position < 0 || position >= alignedSeq[0].Length) throw new ArgumentOutOfRangeException();
            return sumOfPairs(position, costFunc);
        }

        public int GetSumOfPairsTotal(Func<char, char, int> costFunc)
        {
            int result = 0;
            for (int i = 0; i < alignedSeq[0].Length; i++)
                result += sumOfPairs(i, costFunc);
            return result;
        }
    }
}
