﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BBINFtoolbox;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace BBinfToolboxTestPr
{
    [TestClass]
    public class DistanceTesting
    {
        [TestMethod]
        public void AlignmentTest()
        {
            var align = new SeqAlignment("AB", "ABXAB");
            var result = align.GetAllOptimalAlignments();

            Assert.AreEqual(result.Count, 3);
        }

        [TestMethod]
        public void EditDstTest()
        {
            var align = new SeqAlignment("CLOCK", "LACKS");
            int dst = align.EditDst();
            Assert.AreEqual(dst, 3);
        }
    }

    [TestClass]
    public class PDBtesting
    {
        [TestMethod]
        public void PdbHetAtomLoadTest()
        {
            string line = "HETATM12377 MN    MN I 147      75.436 135.124  -0.612  1.00 99.79          MN  ";
            var atom = new PdbAtom(line.Replace('.', ','));
            Assert.AreEqual(atom.Name, "MN");
            Assert.AreEqual(atom.AlternateLoc, ' ');
            Assert.AreEqual(atom.Charge, "");
            Assert.AreEqual(atom.Number, 12377);
            Assert.AreEqual(atom.Occupancy, 1d);
            Assert.AreEqual(atom.TemperatureFactor, 99.79);
            Assert.AreEqual(atom.Type, "MN");
            double[] fields = new double[] { 75.436, 135.124, -0.612 };
            for (int i = 0; i < 3; i++)
            {
                Assert.AreEqual(atom.Coordinates[i], fields[i]);
            }
        }

        [TestMethod]
        public void PdbAtomLoadTest()
        {
            string line = "ATOM    240  OP1  DC I  13      -0.177 105.851  30.464  1.00104.25           O  ";
            var atom = new PdbAtom(line.Replace('.', ','));
            Assert.AreEqual(atom.Name, "OP1");
            Assert.AreEqual(atom.AlternateLoc, ' ' );
            Assert.AreEqual(atom.Charge, "");
            Assert.AreEqual(atom.Number, 240);
            Assert.AreEqual(atom.Occupancy, 1d);
            Assert.AreEqual(atom.TemperatureFactor, 104.25);
            Assert.AreEqual(atom.Type, "O");
            double[] fields = new double[] { -0.177, 105.851, 30.464 };
            for (int i = 0; i < 3; i++)
            {
                Assert.AreEqual(atom.Coordinates[i], fields[i]);
            }
        }
    }

    [TestClass]
    public class FASTAtesting
    {
        

        [TestMethod]
        public void FastaLoadDescTest()
        {
            var item = FastaParser.Parse("3q6k.fasta.txt");
            Assert.AreEqual(item[0].Description, "3Q6K:A|PDBID|CHAIN|SEQUENCE1");               
        }

        [TestMethod]
        public void FastaGetSubSq()
        {
            var item = FastaParser.Parse("3q6k.fasta.txt")[0];
            var realsq = File.ReadLines("3q6k.fasta.txt");
            var sq = "ADTQGYKWKQLLYNNVTPGSYNPDNMISTAFAYDAEGEKLFLAVPRKLPRVPYTLAEVDTKNSLGVKGKHSPLLNKFSGHKTGKELTSIYQPVIDDCRRLWVVDIGSVEYRSRGAKDYPSHRPAIVAYDLKQPNYPEVVRYYFPTRLVEKPTYFGGFAVDVANPKGDCSETFVYITNFLRGALFIYDHKKQDSWNVTHPTFKAERPTKFDYGGKEYEFKAGIFGITLGDRDSEGNRPAYYLAGSAIKVYSVNTKELKQKGGKLNPELLGNRGKYNDAIALAYDPKTKVIFFAEANTKQVSCWNTQKMPLRMKNTDVVYTSSRFVFGTDISVDSKGGLWFMSNGFPPIRKSEKFKYDFPRYRLMRIMDTQEAIAGTACDMNA";

            //nonoverlaping
            var subsq = item.GetSubsequence(115, 14);
            Assert.AreEqual(sq.Substring(115, 14), subsq);

            
        }

        [TestMethod]
        public void FastaLoadSequenceTest()
        {
            var item = FastaParser.Parse("3q6k.fasta.txt")[0];
            var sq = "ADTQGYKWKQLLYNNVTPGSYNPDNMISTAFAYDAEGEKLFLAVPRKLPRVPYTLAEVDTKNSLGVKGKHSPLLNKFSGHKTGKELTSIYQPVIDDCRRLWVVDIGSVEYRSRGAKDYPSHRPAIVAYDLKQPNYPEVVRYYFPTRLVEKPTYFGGFAVDVANPKGDCSETFVYITNFLRGALFIYDHKKQDSWNVTHPTFKAERPTKFDYGGKEYEFKAGIFGITLGDRDSEGNRPAYYLAGSAIKVYSVNTKELKQKGGKLNPELLGNRGKYNDAIALAYDPKTKVIFFAEANTKQVSCWNTQKMPLRMKNTDVVYTSSRFVFGTDISVDSKGGLWFMSNGFPPIRKSEKFKYDFPRYRLMRIMDTQEAIAGTACDMNA";

            Assert.AreEqual(item.Sequence, sq);
        }
    }
}
